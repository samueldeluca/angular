"use strict";
exports.__esModule = true;
var rxjs_1 = require("rxjs");
console.log(1);
var observable = new rxjs_1.Observable(function (observer) {
    console.log('OBS1');
    setTimeout(function () {
        console.log('OBS2');
        observer.next('Hello from observable');
    }, 5000);
});
console.log(2);
observable.subscribe(function (value) { return console.log(value); });
console.log(3);
/*
L'ordine di plot nella console è: 1, 2, OBS1, 3, OBS2, Hello from observable
*/ 
