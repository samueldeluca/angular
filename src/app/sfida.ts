import { Observable } from "rxjs";

console.log(1);

const observable = new Observable(observer => {

    console.log('OBS1');
    setTimeout(() => {
        console.log('OBS2');
        observer.next('Hello from observable');
    }, 5000 );

});

console.log(2);

observable.subscribe(value => console.log(value));

console.log(3);

/*
L'ordine di plot nella console è: 1, 2, OBS1, 3, OBS2, Hello from observable
*/