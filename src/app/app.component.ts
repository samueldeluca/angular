import { Component } from '@angular/core';
import { Observable, observable } from 'rxjs';
import { HEROES } from './_models/mock-heroes';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'observables';
 
  heroes: string[] = HEROES;

  // logica per il filtro
  filteredHeroes : string[] = this.heroes.slice();

  filterHeroesBySearch(eventString: string){

    if (!eventString) {
      this.heroes = this.filteredHeroes;
    } else {
      this.heroes = this.filteredHeroes.
                        filter(element => element.includes(eventString));
    }
  }

constructor(){
  /*
  const observable = new Observable(observer =>{
    observer.next("Hello from Observable");
  });
  observable.subscribe(value => console.log(value));
  */

  /*
 const observable = new Observable(observer => {
	observer.next(1);
	observer.next(2);
	observer.error(Error);
	observer.next(3);
	observer.complete();
});

observable.subscribe(
	value => console.log(value),
	error => console.log(error),
  () => console.log("completed")
); */
}

}